import { gUserInfo } from './info'

function App() {
  return (
    <div>
      <h5>Họ và tên: {gUserInfo.lastname + ' ' + gUserInfo.firstname} </h5>
      <img src={gUserInfo.avatar} width={300}></img>
      <p>Tuổi: {gUserInfo.age}</p>
      <p>{gUserInfo.age >= 35 ? 'Anh ấy đã già' : 'Anh ấy còn trẻ'}</p>
      <ul>
        {gUserInfo.language.map(function (element, index) {
          return <li> {element} </li>
        })}
      </ul>

    </div>
  );
}

export default App;

