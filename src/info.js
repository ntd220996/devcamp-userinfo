import  imageavatar  from './Assets/Images/avatardefault_92824.png'

export const gUserInfo = {
    firstname: 'Hoang',
    lastname: 'Pham',
    avatar: imageavatar,
    age: 30,
    language: ['Vietnamese', 'Japanese', 'English']
}

